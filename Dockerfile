FROM node:fermium

RUN mkdir -p /app
COPY * /app/

WORKDIR /app

RUN yarn
RUN yarn nuxt build

ENTRYPOINT [ "yarn", "nuxt", "start" ]